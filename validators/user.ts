import Joi = require('joi');

const signup = Joi.object().keys({
  name: Joi.string().alphanum().min(3).max(30),
  lastName: Joi.string().alphanum().min(3).max(30),
  password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/),
  age: Joi.number().positive().min(10).max(100),
  email: Joi.string().email(),
  role: Joi.number().positive().min(0).max(10)
});

const login = Joi.object().keys({
  email: Joi.string().email(),
  password: Joi.string()
});

export {signup, login};
