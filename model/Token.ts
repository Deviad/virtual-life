import {Entity, PrimaryGeneratedColumn, Column, BaseEntity, PrimaryColumn, Index, ManyToOne, JoinColumn} from 'typeorm';
import {IUser, User} from './User';
import {IGenericEntity} from './IGenericEntity';

export interface IToken extends IGenericEntity<IUser> {
  id?: number;
  token: string;
  user?: User;
}

@Entity('tokens')
export class Token extends BaseEntity {

  @PrimaryGeneratedColumn()
  public id: number;

  @Column({type: 'varchar', nullable: false})
  @Index({ unique: true })
  public token: string;

  @Column({ type: 'int', nullable: false })
  @ManyToOne(type => User, user => user.tokens)
  @JoinColumn({
    name: 'user'})
  public user: User;



  public create(token: IToken) {
    const _token = new Token();
          _token.token = token.token;
          _token.user = token.user;
    return _token;
  }
}

