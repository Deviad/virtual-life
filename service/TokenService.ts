import {injectable} from 'inversify';
import { TypeORMCLient } from '../utils/sqldb/client';
import { User } from '../model/User';

@injectable()
class TokenService extends TypeORMCLient<User> {
  constructor() {
    super(User);
  }
}

export default TokenService;
