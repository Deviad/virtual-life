import bcrypt = require('bcrypt');
import {injectable} from 'inversify';

@injectable()
class PasswordService {

  public hashPassword(password): Promise<any> {
     const saltRounds: number = 10;
        return bcrypt.genSalt(saltRounds)
          .then(salt => bcrypt.hash(password, salt));
  }
}

export default PasswordService;
