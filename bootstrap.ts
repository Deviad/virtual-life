import 'reflect-metadata';
import { InversifyExpressServer } from 'inversify-express-utils';
import {Container} from 'inversify';
import { makeLoggerMiddleware } from 'inversify-logger-middleware';
import * as bodyParser from 'body-parser';
import * as helmet from 'helmet';
import TYPES from './constant/types';
import { UserService, PasswordService, TokenService} from './service';
import { TypeORMCLient } from './utils/sqldb/client';
import './controller/home';
import './controller/UserController';
import {User, Token} from './model';
import {UserController} from './controller/UserController';
import config from './config';

const { port } = config.server;

// load everything needed to the Container
let container = new Container();

if (process.env.NODE_ENV === 'development') {
    let logger = makeLoggerMiddleware();
    container.applyMiddleware(logger);
}

container.bind<TypeORMCLient<User>>(TYPES.UserService).to(UserService).whenInjectedInto(UserController);
container.bind<TypeORMCLient<Token>>(TYPES.TokenService).to(TokenService).whenInjectedInto(UserController);
container.bind(PasswordService).to(PasswordService).whenInjectedInto(UserController);

// start the server
let server = new InversifyExpressServer(container);
server.setConfig((app) => {
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(bodyParser.json());
  // app.use(helmet());
  // app.use(jwt({
  //   secret: config.server.secret,
  //   credentialsRequired: false,
  //   getToken: function fromHeaderOrQuerystring (req) {
  //     if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
  //       return req.headers.authorization.split(' ')[1];
  //     } else if (req.query && req.query.token) {
  //
  //       return req.query.token;
  //     }
  //     return null;
  //   }
  // }));
});

let app = server.build();
app.listen(port);
console.log('Server started on port 3000 :)');

exports = module.exports = app;
