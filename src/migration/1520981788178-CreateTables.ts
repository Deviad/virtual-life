import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateTables1520981788178 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `users` (`id` int(11) NOT NULL AUTO_INCREMENT, `email` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `name` varchar(255) NOT NULL, `lastName` varchar(255) NOT NULL, `age` int(11) NOT NULL, `role` int(11) NOT NULL, `tokens` json, UNIQUE INDEX `IDX_97672ac88f789774dd47f7c8be` (`email`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `tokens` (`id` int(11) NOT NULL AUTO_INCREMENT, `token` varchar(255) NOT NULL, `user` int(11) NOT NULL, UNIQUE INDEX `IDX_6a8ca5961656d13c16c04079dd` (`token`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `tokens` ADD CONSTRAINT `FK_20a1c32e04c1bde78d3f277ba6e` FOREIGN KEY (`user`) REFERENCES `users`(`id`)");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `tokens` DROP FOREIGN KEY `FK_20a1c32e04c1bde78d3f277ba6e`");
        await queryRunner.query("DROP INDEX `IDX_6a8ca5961656d13c16c04079dd` ON `tokens`");
        await queryRunner.query("DROP TABLE `tokens`");
        await queryRunner.query("DROP INDEX `IDX_97672ac88f789774dd47f7c8be` ON `users`");
        await queryRunner.query("DROP TABLE `users`");
    }

}
