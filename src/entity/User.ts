import {Entity, PrimaryGeneratedColumn, Column, Index, OneToMany} from "typeorm";
import {Token} from './Token';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({nullable: false})
  @Index({ unique: true })
  public email: string;

  @Column({nullable: false})
  public password: string;

  @Column({nullable: false})
  public name: string;

  @Column({nullable: false})
  public lastName: string;

  @Column({type: 'int', nullable: false})
  public age: number;

  @Column({type: 'int', nullable: false})
  public role: number;

  @Column({ type: 'json', nullable: true })
  @OneToMany(
    type => Token,
    token => token.user, {
      cascade: ['update'],
      eager: true
    })
  public tokens: Token[];

}
