import {Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {User} from "./User";

@Entity('tokens')
export class Token {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({type: 'varchar', nullable: false})
  @Index({ unique: true })
  public token: string;

  @Column({ type: 'int', nullable: false })
  @ManyToOne(type => User, user => user.tokens)
  @JoinColumn({
    name: 'user'})
  public user: User;
}
