import * as express from 'express';
import {
  interfaces,
  controller,
  httpGet,
  httpPost,
  httpPut,
  httpDelete,
  request,
  queryParam,
  response,
  requestParam, BaseHttpController
} from 'inversify-express-utils';
import {inject, injectable} from 'inversify';
import jwtservice = require('jsonwebtoken');
import jwt = require('express-jwt');
import {signup, login, Joi} from '../validators/';
import {IToken, Token, User} from '../model';
import TYPES from '../constant/types';
import {UserService, PasswordService, TokenService} from '../service';
import config from '../config';
import {ExpiredToken, NotEnoughPrivileges} from './errors';

import CircularJSON = require('circular-json');
import {getConnection} from "typeorm";

/* JSON responses are compliant with Jsend specifications
   https://labs.omniti.com/labs/jsend
*/



@controller('/user', jwt({secret: config.server.secret}).unless({path: ['/user/login', '/user/signup']}))

export class UserController extends BaseHttpController {
  private secret: string;

  constructor(@inject(TYPES.UserService) private userService: UserService,
              @inject(TYPES.TokenService) private tokenService: TokenService,
              @inject(PasswordService) private passwordService: PasswordService) {
    super();
    this.secret = config.server.secret;
  }

  @httpGet('/')
  public async getUsers(@request() req: any, @response() res: express.Response): Promise<void> {
    try {
      await this.isAuthorized(0, req.user, req);
      res.status(200).json({status: 'success', data: await this.userService.find() as void | User[]});
    } catch (err) {
      res.status(400).json({status: 'error', message: err.message});
    }
  }

  @httpPost('/login')
  public async login(@request() req: express.Request, @response() res: express.Response): Promise<void> {
    try {
      const schema = login.with('password', 'email');
      await Joi.validate(req.body, schema).then(async (data) => {
        const user = await this.userService.findOneByOrFail({where: {email: data.email}}) as User;
        if (user.password === req.body.password) {
          const tokenString = jwtservice.sign({email: user.email, role: user.role}, this.secret, {expiresIn: 31104000});
          let token = new Token();
          token.token = tokenString;
          token.user = user;
          await this.tokenService.save(null, token);
          // const tokens = await this.tokenService.find(Token, {where: {user: user.id}});
          // await this.userService.update(user.id, {tokens});

          // console.log('TOKENS', tokens);

          res.status(200).json({status: 'success', data: tokenString});
        }
      });
    } catch (err) {
      res.status(400).json({status: 'error', message: err.message});
    }
  }

  @httpGet('/:id')
  public async getUser(@request() req: express.Request, @response() res: express.Response): Promise<void> {
    try {
      res.status(200).json({
        data: await this.userService.findOneOrFail(req.params.id) as void | User,
        status: 'success'
      });
    } catch (err) {
      res.status(400).json({status: 'error', message: err.message});
    }
  }

  @httpPost('/signup')
  public async newUser(@request() req: express.Request, @response() res: express.Response): Promise<void> {
    try {
      const schema = signup.with('email', ['name', 'lastName', 'password']);
      await Joi.validate(req.body, schema)
        .then(async (user: User) => {
          const userWithHashedPassword = JSON.parse(JSON.stringify(user)); // deep clone object not having dates without using libraries
          userWithHashedPassword.password = await this.passwordService.hashPassword(userWithHashedPassword.password);
          return userWithHashedPassword;
        })
        .then(async (user: User) => {
          user.role = 1;
          console.log(user);
          const result = await this.userService.save(user);
          res.status(201).json({status: 'success', data: result});
        })
        .catch(err => res.status(400).json({status: 'error', message: err.message}));
    } catch (err) {
      res.status(400).json({status: 'error', message: err.message});
    }
  }

  @httpPut('/:id')
  public async updateUser(@request() req: express.Request, @response() res: express.Response): Promise<void> {
    try {
      await this.userService.update(req.params.id, req.body)
        .then(async () => {
          const result = await this.userService.findOneOrFail(req.params.id) as void | User;
          res.json({status: 'success', data: result});
        });
    } catch (err) {
      res.status(400).json({error: err.message});
    }
  }

  @httpDelete('/:id')
  public async deleteUser(@request() req: express.Request, @response() res: express.Response): Promise<any> {
    try {
      return await this.userService.findOneOrFail(req.params.id)
        .then(entity => {
          if (entity) {
            return this.userService.remove(entity);
          }
        })
        .then(() => {
          res.status(200).json({status: 'success', data: null});
        });
    } catch (err) {
      res.status(400).json({status: 'error', message: err.message});
    }
  }

  // private async isExisting(id) {
  //   return await this.userService.findOneById(id)
  //     .then(value => {
  //       if (!value) {
  //        throw new NoEntityFoundWithId(id);
  //       }
  //     });
  // }

    private async isAuthorized(authRole, {email, role}, req) {

      const user = await this.userService.findOneByOrFail({where: {email}}) as User;
      // let tokens: string[] = await getConnection()
      //   .createQueryBuilder(Token, "token")
      //   .where("user = :id", { id: user.id })
      //   .getMany()
      //   .then((tokens: Token[]) => {
      //     return tokens.map(value => {
      //       const { token } = value;
      //       return token;
      //     });
      //   });
        const tokens = user.tokens;
        const past = tokens.slice(0, -1); // we remove the current login token
        const currentToken = req.headers.authorization.split(' ')[1];
        const oldTokens = past.map(res => {
          const {token, ...rest} = res;
          return token;
        });
      // console.log('old tokens>>>>', oldTokens);
      // console.log('current token>>>>', currentToken);
        if (oldTokens.indexOf(currentToken) !== -1) {
          throw new ExpiredToken();
        }
        if (authRole !== role) { //
          throw new NotEnoughPrivileges();
        }
        return true;
    }
}
